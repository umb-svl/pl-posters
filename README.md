Goal: Create posters with succinct programming language theory to be reference
or casual introduction for students. A rule of thumb for selecting posters is
to facilitate a student's ability to decipher programming language related 
academic papers

== Poster Ideas
- [ ] Untyped Lambda Calculus
- [ ] Simply Lambda Calculus
- [ ] Greek Alphabet
- [ ] Common Math Symbols for CS
- [ ] Reduction Rules
- [ ] Haskell/Scheme Syntax
- [ ] Predicate Logic with Quantifiers

== Puzzle Themes
- [ ] Z3
- [ ] Klee
